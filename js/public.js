//弹窗
function alertImg(){
	document.getElementById("alertbody").style.display = "block";
	document.getElementById("altcontent").style.display = "block";
	altcontent.style.height = "53%";
	document.getElementById("altcontent").innerHTML = '<img src="img/ewm.png"/> <p>扫描二维码，关注公众号</p> <p>一切尽在不言中~</p>';
}
function alertTxt(){
	document.getElementById("alertbody").style.display = "block";
	document.getElementById("altcontent").style.display = "block";
	document.getElementById("altcontent").innerHTML = '<h3>投票成功，今天可投票数剩<b>X</b>票！</h3> <h3>累计投足<i>50票</i>即可免费领取纸质实体书</h3><button onclick="hidealert()">确定</button>';
}
function hidealert(){
	document.getElementById("alertbody").style.display = "none";
	document.getElementById("altcontent").style.display = "none";
}
//倒计时
(function(){
	addtime("countdown","2018-06-28 10:40:00");
	function addtime(ids,time){
		var timer = null;
		for(var a=0;a<arguments.length;a++){
			window[arguments[a]] = time;
			if(a<1){
				function runtime(){
					getRTime(window[arguments[a]],ids);
				}
				getRTime(window[arguments[a]],ids);
				timer = setInterval(runrun,1000);
			}
		}
		function runrun(){
			clearInterval(timer);
			addtime(ids,time);
		}
	}
	function getRTime(endtime,id){
		var EndTime = new Date(endtime.replace(/-/g,'/')).getTime();
		var NowTime = new Date(); 
		var t =EndTime - NowTime.getTime();
		var this_d =id;
	//		console.log(t) 
		//如果日期是过去的时间点，t就是负*-1就相当于秒表计时
		if(t<0){
				t*=-1;
				document.getElementById("timeTxt").innerHTML = "活动结束,投票通道已关闭！";
		}else{
			var d=Math.floor(t/1000/60/60/24);
			var h=Math.floor(t/1000/60/60%24); 
			var m=Math.floor(t/1000/60%60); 
			var s=Math.floor(t/1000%60);
			if(h<10){
				if(h == 0){
					h = "0" + h;
				}else{
					h = "0" + h;
				}
			}
			if(m<10){
				if(m == 0){
					m = "0" + m;
				}else{
					m = "0" + m;
				}
			}
			if(s<10){
				if(s == 0){
					s = "0" + s;
				}else{
					s = "0" + s;
				}
			}
			$('#'+this_d).find(".t_d")[0].innerHTML = d + "天"; //天
			$('#'+this_d).find(".t_h")[0].innerHTML = h + "时"; //时
			$('#'+this_d).find(".t_m")[0].innerHTML = m + "分"; //分
			$('#'+this_d).find(".t_s")[0].innerHTML = s + "秒"; //秒		
		}
	}
		
})()
